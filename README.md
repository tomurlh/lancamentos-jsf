## Lançamentos JSF

Projeto desenvolvido com [JavaServer Faces (JSF)](https://javaserverfaces.github.io/) no front-end que consome uma [API](https://gitlab.com/tomurlh/lancamentos-api) escrita com [Spark Java Framework](http://sparkjava.com) e [JPA](https://docs.oracle.com/javaee/7/api/javax/persistence/package-summary.html)


O projeto foi desenvolvido para realização de estudo comparativo entre Framework Java (JSF) e JavaScript (React.js), tema de monografia da pós-graduação em *Desenvolvimento de Sistemas com Java*.

A aplicação realiza consulta, cadastro, atualização e remoção de pessoas e lançamentos financeiros.

### `front-end`


| Tecnologia  | Descrição |
| ------------- | ------------- |
| ![](https://gitlab.com/tomurlh/lancamentos-jsf/raw/master/src/main/webapp/imagens/jsf-logo-small.jpg)  | [JSF](https://javaserverfaces.github.io/) -> usado para a contrução das views  |
| ![](https://gitlab.com/tomurlh/lancamentos-jsf/raw/master/src/main/webapp/imagens/prime-logo-small.png)  | [PrimeFaces](https://www.primefaces.org/showcase/) -> biblioteca de componentes para o JSF  |
| ![](https://gitlab.com/tomurlh/lancamentos-jsf/raw/master/src/main/webapp/imagens/unirest-logo-small.png)  | [Unirest](https://github.com/Kong/unirest-java) -> ferramenta para consumir APIs, informar o método HTTP e passar parâmetros da rota de forma prática  |
| ![](https://gitlab.com/tomurlh/lancamentos-jsf/raw/master/src/main/webapp/imagens/jackson-imagem-small.png)  | [Jackson](https://github.com/FasterXML/jackson-databind) -> usado para converter as respostas JSON para objetos Java  |

### `back-end`

| Tecnologia  | Descrição |
| ------------- | ------------- |
| ![](https://gitlab.com/tomurlh/lancamentos-jsf/raw/master/src/main/webapp/imagens/spark-logo-small.png)  | [Spark Java Framework](http://sparkjava.com) -> criação das rotas da API, receber as requisições e devolver a resposta  |
| ![](https://gitlab.com/tomurlh/lancamentos-jsf/raw/master/src/main/webapp/imagens/jpa-imagem-small.png)  | [Java Persistence API](https://docs.oracle.com/javaee/7/api/javax/persistence/package-summary.html) -> usado na API para interagir com o banco de dados  |
| ![](https://gitlab.com/tomurlh/lancamentos-jsf/raw/master/src/main/webapp/imagens/jackson-imagem-small.png)  | [Jackson](https://github.com/FasterXML/jackson-databind) -> usado para converter os objetos Java capturados do banco para JSON  |