﻿insert into pessoa (nome) values ('Thiago Medeiros');
insert into pessoa (nome) values ('Adam Smith');
insert into pessoa (nome) values ('Higor Melo');
insert into pessoa (nome) values ('Roni Quiozini');
insert into pessoa (nome) values ('Ruan Arouxa');


insert into lancamento 
    (pessoa_id, descricao, tipo, valor, data_vencimento, data_pagamento, pago) values
    (1, 'Coxinha Dolccini', 'Despesa', 5, '10-16-2018', '10-16-2018', true);

insert into lancamento 
    (pessoa_id, descricao, tipo, valor, data_vencimento, data_pagamento, pago) values
    (1, 'Baur Dolccini', 'Despesa', 5, '10-16-2018', '10-16-2018', true);

insert into lancamento 
    (pessoa_id, descricao, tipo, valor, data_vencimento, data_pagamento, pago) values
    (2, 'Pão-pizza Dolccini', 'Despesa', 5, '10-16-2018', '10-16-2018', true);

insert into lancamento 
    (pessoa_id, descricao, tipo, valor, data_vencimento, data_pagamento, pago) values
    (3, 'Coxinha Dolccini', 'Despesa', 5, '10-16-2018', '10-16-2018', true);
