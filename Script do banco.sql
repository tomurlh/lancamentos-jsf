create table pessoa (
	id serial not null primary key,
	nome varchar(45) not null
);

create table lancamento (
	id serial not null primary key,
	pessoa_id integer not null,
	tipo varchar(20) not null,
	descricao varchar(150) not null,
	valor decimal(10,2) not null,
	data_vencimento date not null,
	pago boolean not null,
	data_pagamento date,
	foreign key (pessoa_id) references pessoa (id)
);