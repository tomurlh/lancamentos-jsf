package conversores;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;

import model.Pessoa;
import util.JpaUtil;

@FacesConverter(forClass=Pessoa.class)
public class PessoaConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Pessoa response = null;
		
		if(value != null) {
			EntityManager em = JpaUtil.getEntityManager();
			response = em.find(Pessoa.class, new Integer(value));
		}
		return response;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if(value != null)
			return ((Pessoa) value).getId().toString();

		return null;
	}

}
