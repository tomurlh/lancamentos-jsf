package testes;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.mashape.unirest.http.exceptions.UnirestException;

import util.ConsumoApi;

public class ConsumoApiTeste {

	public static void main(String[] args) throws UnirestException, JsonParseException, JsonMappingException, IOException {
		ConsumoApi lancamentosApi = new ConsumoApi();
		
//		lancamentosApi.getPessoas();
		lancamentosApi.addPessoa();
	}
	
}
