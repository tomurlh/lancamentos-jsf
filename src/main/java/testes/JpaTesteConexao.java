package testes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JpaTesteConexao {
	public static void main(String[] args) throws SQLException {
		Connection con = null;
		try {
			Class.forName("org.postgresql.Driver");
			System.out.println("driver found");
			System.out.println("connecting...");
			con = DriverManager.getConnection("jdbc:postgresql://localhost/lancamentos", "postgres", "postgres");
			System.out.println("connected");
		}
		catch(Exception e) {
			System.out.println("Cannot connect the database");
			e.printStackTrace();
		}
		finally {
			if(con != null)
				con.close();
		}
	}
}
