package testes;

import java.sql.SQLException;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import model.Pessoa;
import util.JpaUtil;

public class JpaTesteConsulta {
	public static void main(String[] args) throws SQLException {
		EntityManager em = JpaUtil.getEntityManager();
		
		TypedQuery<Pessoa> query = em.createQuery("select p from Pessoa p", Pessoa.class);
		
		for(Pessoa pessoa : query.getResultList()) {
			System.out.println(pessoa.getNome());
		}
	}
}
