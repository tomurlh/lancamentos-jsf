package util;

import java.io.IOException;
import java.util.List;

import javax.faces.bean.ManagedBean;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import model.Pessoa;
import model.Lancamento;

@ManagedBean
public class ConsumoApi {

//	GET /pessoa - OK
//	GET /pessoa/:id
//	POST /pessoa/salvar - OK
//	PUT /pessoa/:id
//	DELETE /pessoa/:id
//
//	GET /lancamento - OK
//	GET /lancamento/:id
//	POST /lancamento/salvar
//	PUT /lancamento/:id
//	DELETE /lancamento/:id

	
	
	/*
	 * GET /pessoa
	 */
	public List<Pessoa> getPessoas() throws UnirestException, IOException {
		HttpResponse<String> response = Unirest.get("http://localhost:4567/pessoa")
			.header("accept", "application/json")
			.asString();
				
		com.fasterxml.jackson.databind.ObjectMapper om = 
				new com.fasterxml.jackson.databind.ObjectMapper();
		
		List<Pessoa> pessoas = 
				om.readValue(response.getBody(), new TypeReference<List<Pessoa>>(){});
		
		System.out.println(pessoas);
		
		return pessoas;
	}
	
	
	
	/*
	 * GET /pessoa/:id
	 */
	public Pessoa getPessoa(String id) throws UnirestException, IOException {
		HttpResponse<String> response = Unirest.get("http://localhost:4567/pessoa/{id}")
			.header("accept", "application/json")
			.asString();
				
		com.fasterxml.jackson.databind.ObjectMapper om = 
				new com.fasterxml.jackson.databind.ObjectMapper();
		
		Pessoa pessoa = 
				om.readValue(response.getBody(), new TypeReference<Pessoa>(){});
		
		System.out.println(pessoa);
		
		return pessoa;
	}
	
	
	
	/*
	 * POST /pessoa/salvar
	 */
	public void addPessoa() throws UnirestException, IOException {
		HttpResponse<String> response = Unirest.post("http://localhost:4567/pessoa/salvar")
				.header("accept", "application/json")
				.field("nome", "Mark")
				.asString();
					
//		com.fasterxml.jackson.databind.ObjectMapper om = 
//				new com.fasterxml.jackson.databind.ObjectMapper();
		
//		System.out.println(
//				om.readValue(response.getBody(), new TypeReference<Pessoa>(){}).toString()
//		);
		System.out.println(response.getBody());
	}
	
	
	
	/*
	 * GET /lancamento
	 */	
	public List<Lancamento> getLancamentos() throws UnirestException, IOException {
		HttpResponse<String> response = Unirest.get("http://localhost:4567/lancamento")
			.header("accept", "application/json")
			.asString();
				
		com.fasterxml.jackson.databind.ObjectMapper om = 
				new com.fasterxml.jackson.databind.ObjectMapper();
		
		List<Lancamento> lancamentos = 
				om.readValue(response.getBody(), new TypeReference<List<Lancamento>>(){});
		
		System.out.println(lancamentos);
		
		return lancamentos;
	}
}
