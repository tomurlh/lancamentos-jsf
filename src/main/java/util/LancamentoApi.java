package util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import model.Lancamento;

public class LancamentoApi {

	private static com.fasterxml.jackson.databind.ObjectMapper om = 
			new com.fasterxml.jackson.databind.ObjectMapper();
	
	
	
	/*
	 * GET /lancamento
	 */
	public static List<Lancamento> obterTodos() throws UnirestException, IOException {
		HttpResponse<String> response = Unirest.get("http://localhost:4567/lancamento")
			.header("accept", "application/json")
			.asString();
				
		return om.readValue(response.getBody(), new TypeReference<List<Lancamento>>(){});
	}
	
	
	
	
	/*
	 * GET /lancamento/:id
	 */
	public static Lancamento obter(String id) throws UnirestException, IOException {
		HttpResponse<String> response = Unirest.get("http://localhost:4567/lancamento/{id}")
				.header("accept", "application/json")
				.routeParam("nome", id)
				.asString();
		
		return om.readValue(response.getBody(), new TypeReference<Lancamento>(){});
	}
	
	
	
	
	
	/*
	 * POST /lancamento/salvar
	 */
	public static Lancamento cadastrar(Lancamento lancamento) throws UnirestException, IOException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		
		HttpResponse<String> response = Unirest.post("http://localhost:4567/lancamento/salvar")
			.header("accept", "application/json")
			.field("pessoaId", lancamento.getPessoa().getId().toString())
			.field("tipo", lancamento.getTipo())
			.field("descricao", lancamento.getDescricao())
			.field("valor", lancamento.getValor().toString())
			.field("pago", lancamento.isPago())
			.field("dataVencimento", formatter.format(lancamento.getDataVencimento()))
			.asString();

		return om.readValue(response.getBody(), new TypeReference<Lancamento>(){});
	}
	
	
	/*
	 * PUT /lancamento/:id
	 */
	public static Lancamento atualizar(Lancamento lancamento) throws UnirestException, IOException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		
		HttpResponse<String> response = Unirest.put("http://localhost:4567/lancamento/{id}")
				.header("accept", "application/json")
				.routeParam("id", lancamento.getId().toString())
				.field("pessoaId", lancamento.getPessoa().getId().toString())
				.field("tipo", lancamento.getTipo())
				.field("descricao", lancamento.getDescricao())
				.field("valor", lancamento.getValor().toString())
				.field("pago", lancamento.isPago())
				.field("dataVencimento", formatter.format(lancamento.getDataVencimento()))
				.asString();
			
		return om.readValue(response.getBody(), new TypeReference<Lancamento>(){});
	}
	
	
	
	/*
	 * DELETE /lancamento/:id
	 */
	public static Lancamento remover(String id) throws UnirestException, IOException {
		HttpResponse<String> response = Unirest.delete("http://localhost:4567/lancamento/{id}")
				.header("accept", "application/json")
				.routeParam("id", id)
				.asString();
		
		return om.readValue(response.getBody(), new TypeReference<Lancamento>(){});
	}
}