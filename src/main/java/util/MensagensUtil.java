package util;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import model.Lancamento;
import model.Pessoa;

public class MensagensUtil {
	public static void checarPessoa(Pessoa pessoa, String msgSucesso, String msgErro) {
		if(pessoa.getId() != null) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, msgSucesso, msgSucesso));			
		}
		else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_FATAL, msgErro, msgErro));
		}
	}

	
	
	public static void checarLancamento(Lancamento lancamento, String msgSucesso, String msgErro) {
		if(lancamento.getId() != null) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, msgSucesso, msgSucesso));			
		}
		else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_FATAL, msgErro, msgErro));
		}
	}
}
