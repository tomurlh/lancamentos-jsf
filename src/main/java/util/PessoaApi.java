package util;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import model.Pessoa;

public class PessoaApi {

	private static com.fasterxml.jackson.databind.ObjectMapper om = 
			new com.fasterxml.jackson.databind.ObjectMapper();
	
	
	
	/*
	 * GET /pessoa
	 */
	public static List<Pessoa> obterTodos() throws UnirestException, IOException {
		HttpResponse<String> response = Unirest.get("http://localhost:4567/pessoa")
			.header("accept", "application/json")
			.asString();
				
		return om.readValue(response.getBody(), new TypeReference<List<Pessoa>>(){});
	}
	
	
	
	
	/*
	 * GET /pessoa/:id
	 */
	public static Pessoa obter(String id) throws UnirestException, IOException {
		HttpResponse<String> response = Unirest.get("http://localhost:4567/pessoa/{id}")
				.header("accept", "application/json")
				.routeParam("nome", id)
				.asString();
		
		return om.readValue(response.getBody(), new TypeReference<Pessoa>(){});
	}
	
	
	
	
	
	/*
	 * POST /pessoa/salvar
	 */
	public static Pessoa cadastrar(Pessoa pessoa) throws UnirestException, IOException {		
		HttpResponse<String> response = Unirest.post("http://localhost:4567/pessoa/salvar")
			.header("accept", "application/json")
			.field("nome", pessoa.getNome())
			.asString();

		return om.readValue(response.getBody(), new TypeReference<Pessoa>(){});
	}
	
	
	/*
	 * PUT /pessoa/:id
	 */
	public static Pessoa atualizar(Pessoa pessoa) throws UnirestException, IOException {
		HttpResponse<String> response = Unirest.put("http://localhost:4567/pessoa/{id}")
				.header("accept", "application/json")
				.routeParam("id", pessoa.getId().toString())
				.field("nome", pessoa.getNome())
				.asString();
			
		return om.readValue(response.getBody(), new TypeReference<Pessoa>(){});		
	}
	
	
	
	/*
	 * DELETE /pessoa/:id
	 */
	public static Pessoa remover(String id) throws UnirestException, IOException {
		HttpResponse<String> response = Unirest.delete("http://localhost:4567/pessoa/{id}")
				.header("accept", "application/json")
				.routeParam("id", id)
				.asString();
		
		return om.readValue(response.getBody(), new TypeReference<Pessoa>(){});
	}
}