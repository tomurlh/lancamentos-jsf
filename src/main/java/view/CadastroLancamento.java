package view;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.mashape.unirest.http.exceptions.UnirestException;

import model.Lancamento;
import model.Pessoa;
import model.TipoLancamento;
import util.JpaUtil;
import util.LancamentoApi;
import util.MensagensUtil;

@ManagedBean
@ViewScoped
public class CadastroLancamento implements Serializable {

	private static final long serialVersionUID = 1L;
	EntityManager em;
	private List<Pessoa> pessoas = new ArrayList<Pessoa>();
	private Lancamento lancamento = new Lancamento();

	@PostConstruct
	public void init() {
		em = JpaUtil.getEntityManager();
		
		TypedQuery<Pessoa> query = em.createQuery("select p from Pessoa p", Pessoa.class);
		
		this.pessoas = query.getResultList();
	}
	
	
	
	/*
	 * POST /lancamento/salvar
	 */
	public void cadastrar() throws UnirestException, IOException {
		Lancamento lancamento = LancamentoApi.cadastrar(this.lancamento);
		
		MensagensUtil.checarLancamento(
				lancamento, "Cadastro efetuado com sucesso", "Houve um erro no cadastro"
		);
	}
	
	
	
	public TipoLancamento[] getTiposLancamentos() {
		return TipoLancamento.values();
	}

	public Lancamento getLancamento() {
		return lancamento;
	}

	public List<Pessoa> getPessoas() {
		return pessoas;
	}
	
}