package view;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;

import com.mashape.unirest.http.exceptions.UnirestException;

import model.Pessoa;
import model.TipoPessoa;
import util.MensagensUtil;
import util.PessoaApi;

@ManagedBean
@ViewScoped
public class CadastroPessoa implements Serializable {

	private static final long serialVersionUID = 1L;
	EntityManager em = null;
	private Pessoa pessoa = new Pessoa();

	@PostConstruct
	public void init() {}
	
	
	
	/*
	 * POST /pessoa/salvar
	 */
	public void cadastrar() throws UnirestException, IOException {
		
		Pessoa pessoa = PessoaApi.cadastrar(this.pessoa);
		
		MensagensUtil.checarPessoa(
				pessoa, "Cadastro efetuado com sucesso", "Houve um erro no cadastro"
		);
	}
	

	public TipoPessoa[] getTiposLancamentos() {
		return TipoPessoa.values();
	}

	public Pessoa getPessoa() {
		return pessoa;
	}
}
