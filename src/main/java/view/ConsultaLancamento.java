package view;

import java.io.IOException;
import java.util.List;

import javax.faces.bean.ManagedBean;

import com.mashape.unirest.http.exceptions.UnirestException;

import model.Lancamento;
import util.LancamentoApi;
import util.MensagensUtil;

@ManagedBean
public class ConsultaLancamento {
		
	/*
	 * GET /lancamento
	 */	
	public List<Lancamento> getLancamentos() throws UnirestException, IOException {		
		return LancamentoApi.obterTodos();
	}
	
	
	
	/*
	 * DELETE /lancamento/:id
	 */
	public void remover(Integer id) throws UnirestException, IOException {
		Lancamento lancamento = LancamentoApi.remover(id.toString());
		
		MensagensUtil.checarLancamento(
				lancamento, "Registro removido com sucesso", "Houve um erro na remoção"
		);
	}
}
