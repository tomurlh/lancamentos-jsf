package view;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.mashape.unirest.http.exceptions.UnirestException;

import model.Pessoa;
import util.MensagensUtil;
import util.PessoaApi;

@ManagedBean
@SessionScoped
public class ConsultaPessoa {
	
	private Pessoa pessoaSelecionada;
	
	@PostConstruct
	public void init() {}
	
	/*
	 * GET /pessoa
	 */
	public List<Pessoa> getPessoas() throws UnirestException, IOException {		
		return PessoaApi.obterTodos();
	}
	
	
	
	/*
	 * GET /pessoa/:id
	 */
	public void getPessoa() {
		System.out.println(this.pessoaSelecionada.getNome());
	}
	

	
	/*
	 * DELETE /pessoa/:id
	 */
	public void remover(Integer id) throws UnirestException, IOException {
		Pessoa pessoa = PessoaApi.remover(id.toString());
		
		MensagensUtil.checarPessoa(
				pessoa, "Registro removido com sucesso", "Houve um erro na remoção"
		);
	}
	
	
	public Pessoa getPessoaSelecionada() {
		return this.pessoaSelecionada;
	}
	public void setPessoaSelecionada(Pessoa pessoaSelecionada) {
		this.pessoaSelecionada = pessoaSelecionada;
	}
}
