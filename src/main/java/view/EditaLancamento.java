package view;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.mashape.unirest.http.exceptions.UnirestException;

import model.Lancamento;
import model.Pessoa;
import model.TipoLancamento;
import util.MensagensUtil;
import util.JpaUtil;
import util.LancamentoApi;

@ManagedBean
@SessionScoped
public class EditaLancamento {
	
	private Lancamento lancamentoSelecionado;
	EntityManager em;
	private List<Pessoa> pessoas;

	@PostConstruct
	public void init() {
		em = JpaUtil.getEntityManager();
		
		TypedQuery<Pessoa> query = em.createQuery("select p from Pessoa p", Pessoa.class);
		
		this.pessoas = query.getResultList();
	}	
	
	
	
	
	/*
	 * GET /lancamento/:id
	 */
	public void getLancamento() {
		System.out.println(this.lancamentoSelecionado.getDescricao());
	}
	
	
	
	/*
	 * PUT /pessoa/:id
	 */
	public void atualizar() throws UnirestException, IOException {
		Lancamento lancamento = LancamentoApi.atualizar(this.lancamentoSelecionado);
		
		MensagensUtil.checarLancamento(
				lancamento, "Registro atualizado com sucesso", "Houve um erro na atualização"
		);
	}
	
	
	
	public Lancamento getLancamentoSelecionado() {
		return this.lancamentoSelecionado;
	}
	public void setLancamentoSelecionado(Lancamento lancamentoSelecionado) {
		this.lancamentoSelecionado = lancamentoSelecionado;
	}
	public List<Pessoa> getPessoas() {
		return this.pessoas;
	}
	public TipoLancamento[] getTiposLancamentos() {
		return TipoLancamento.values();
	}
}
