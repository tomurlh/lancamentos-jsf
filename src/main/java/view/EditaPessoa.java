package view;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.mashape.unirest.http.exceptions.UnirestException;

import model.Pessoa;
import util.MensagensUtil;
import util.PessoaApi;

@ManagedBean
@SessionScoped
public class EditaPessoa {
	
	private Pessoa pessoaSelecionada = new Pessoa();
	
	@PostConstruct
	public void init() {}
	
	
	
	/*
	 * GET /pessoa/:id
	 */
	public void getPessoa() {
		System.out.println(this.pessoaSelecionada.getNome());
	}
	
	
	
	/*
	 * PUT /pessoa/:id
	 */
	public void atualizar() throws UnirestException, IOException {
		System.out.println(pessoaSelecionada.getNome());
		Pessoa pessoa = PessoaApi.atualizar(this.pessoaSelecionada);
		
		MensagensUtil.checarPessoa(
				pessoa, "Registro atualizado com sucesso", "Houve um erro na atualização"
		);
	}
	
	
	
	public Pessoa getPessoaSelecionada() {
		return this.pessoaSelecionada;
	}
	public void setPessoaSelecionada(Pessoa pessoaSelecionada) {
		this.pessoaSelecionada = pessoaSelecionada;
	}
}
